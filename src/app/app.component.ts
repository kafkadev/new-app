import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Nav, Platform, MenuController, ViewController } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NP, PLATFORM } from './../interfaces/state-manage';


//PROVIDERS
import { Events } from 'ionic-angular';
import { EventProvider } from './../providers/event/event';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = "HomePage";
  pages: Array<{ title: string, component: any }>;
  userPages: Array<{ title: string, component: any, mypageid: any }>;
  logoutPages: Array<{ title: string, component: any }>;
  currentPage: string
  constructor(
    public eventProvider: EventProvider,
    public events: Events,
    public platform: Platform,
    public menuCtrl: MenuController,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen
  ) {




    this.events.publish('init:app')
    this.initializeApp();
    this.pages = [
      { title: "HomePage", component: 'HomePage' },
      { title: "SportsPage", component: 'SportsPage' },
      { title: "AboutPage", component: 'About' }
    ];
    this.userPages = [
      { title: "Hesabım", component: 'Account', mypageid : 'home'},
      { title: "Kuponlar", component: 'Account', mypageid : 'coupons'},
      { title: "Mesajlar", component: 'Account', mypageid : 'tickets'},
    ];
    this.logoutPages = [
      { title: "Login", component: 'Login' }
    ];
  }

  initializeApp() {


    this.platform.ready().then(() => {
      PLATFORM['portrait'] = this.platform.isPortrait()
      this.statusBar.styleDefault();
     this.splashScreen.hide();
    });
  }

  openPage(page) {
    this.menuCtrl.close()
    this.nav.setRoot(page.component);
  }

  logout() {
    this.events.publish('user:logout')
    this.menuCtrl.close()
    this.nav.setRoot('Login');
  }

  acBunu = (sport: any) => {
    this.menuCtrl.close()
    this.nav.setRoot('SportDetail', { sportid: sport.id, type: 'sportid' });
  }
  accountMenu(mypageid){
    this.nav.setRoot('Account', {mypageid : mypageid});
    this.menuCtrl.close()
  }

  ngAfterViewInit() {
    this.nav.viewDidLoad.subscribe(item => {
      const viewController = item as ViewController;
      const { name, id, data } = viewController;
      this.events.publish('nav:update', data)
      NP.current = id
      NP.data = data
      console.log(name, id, data);
      let arrPages: any[string] = [
        'sportid',
        'categoryid',
        'tournamentid',
        'liveid',
        'preid',
        'SportsPage',
        'spor-bahisleri'
      ]

      if (
        arrPages.includes(Object.keys(data)[0])
        || arrPages.includes(id)
        || arrPages.includes(name)) {
        this.events.publish('socket:open')
      } else {
        this.events.publish('socket:close')
      }
    })
  }
}
