import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { LOCALE_ID } from '@angular/core';
import { IonicStorageModule } from '@ionic/storage'
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';

//COMPONENTS
import { ComponentsModule } from '../components/components.module';
import { PipesModule } from './../pipes/pipes.module';

//APP
import { MyApp } from './app.component';

//PAGES
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { SportsPage } from '../pages/sports/sports';
import { AboutPage } from "../pages/about/about";
import { AccountPage } from "../pages/account/account";
import { LoginPage } from "../pages/login/login";
import { SignupPage } from "../pages/signup/signup";
import { PopoverPage } from "../pages/about-popover/about-popover";
import { CouponsPage } from "../pages/coupons/coupons";

//NATIVE
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';



//PROVIDERS
import { ServiceApiProvider } from '../providers/service-api/service-api';
import { UserData } from "../providers/user-data";
import { SocketService } from "../providers/socket.service";
import { ActionCoupons } from '../providers/actions-coupons';
import { GeneralProvider } from '../providers/general/general';
import { EventProvider } from '../providers/event/event';
import { WebsocketService } from '../providers/web-socket/web-socket';
//https://live.redsoftnv.com
const config: SocketIoConfig = {
  url: '//x3k62ge.x.incapdns.net',
  options: {
    /* reconnection : true,
     reconnectionAttempts : 3,
     timeout : 10,
     autoConnect : true
     */
  }
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    SportsPage,
    AboutPage,
    AccountPage,
    LoginPage,
    SignupPage,
    PopoverPage,
    CouponsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    PipesModule,
    ComponentsModule,
    IonicModule.forRoot(MyApp, {
      locationStrategy: 'hash'
    }, {
        links: [
          { component: HomePage, name: 'HomePage', segment: '' },
          { component: SportsPage, name: 'SportsPage', segment: 'spor-bahisleri' },
          { component: SportsPage, name: 'SportDetail', segment: 'sport/:sportid' },
          { component: SportsPage, name: 'CategoryDetail', segment: 'sport/category/:categoryid' },
          { component: SportsPage, name: 'TournamentDetail', segment: 'sport/tournament/:tournamentid' },
          { component: SportsPage, name: 'LiveDetail', segment: 'sport/live/:liveid' },
          { component: SportsPage, name: 'PreDetail', segment: 'sport/prematch/:preid' },
          { component: ListPage, name: 'List', segment: 'list' },
          { component: AccountPage, name: 'Account', segment: 'account/:mypageid' },
          { component: LoginPage, name: 'Login', segment: 'login' },
          { component: SignupPage, name: 'Signup', segment: 'signup' },
          { component: AboutPage, name: 'About', segment: 'about' },
          { component: CouponsPage, name: 'CouponsPage', segment: 'current-coupons'}
        ]
      }),
    IonicStorageModule.forRoot(),
    SocketIoModule.forRoot(config)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    SportsPage,
    AboutPage,
    AccountPage,
    LoginPage,
    SignupPage,
    PopoverPage,
    CouponsPage
  ],
  providers: [
    UserData,
    ServiceApiProvider,
    ActionCoupons,
    SocketService,
    StatusBar,
    SplashScreen,
    GeneralProvider,
    EventProvider,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: LOCALE_ID, useValue: localStorage.getItem('locale') },
    WebsocketService,
  ]
})
export class AppModule {
  constructor() {
    localStorage.setItem('locale', 'tr-TR')
  }
}
