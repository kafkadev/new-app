import { Component, OnInit, Input } from '@angular/core';
import { LiveStarted, LiveUpdated } from "../../interfaces/live-state";
import { ActionCoupons } from './../../providers/actions-coupons';
import * as _ from 'underscore';
import * as state from "../../interfaces/state-manage";
@Component({
  selector: 'live-detail',
  templateUrl: 'live-detail.html'
})
export class LiveDetailComponent implements OnInit {
  @Input() liveid: any
  un: any = _
  state: any = state
  liveStarted: any = LiveStarted
  updated: any = LiveUpdated
  expanded: any = {};
  constructor(
    public ACP: ActionCoupons,
  ) { }

  ngOnInit() { }
  toggleAccordion(exp) {
    this.expanded[exp] = !this.expanded[exp]
  }
}
