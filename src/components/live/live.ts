import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LiveStarted, LiveUpdated, LiveCounts } from "../../interfaces/live-state";
import * as state from "../../interfaces/state-manage";
import * as _ from 'underscore';

@Component({
  selector: 'live',
  templateUrl: './live.html'
})
export class LiveComponent {
  state: any = state
  liveStarted: any = LiveStarted
  updated: any = LiveUpdated
  LiveCounts:any = LiveCounts
  liveSports: any = {}
  upcomingMatches: any = {}
  un = _
  constructor(
    public navParams: NavParams,
    public storage: Storage,
  ) {
   // state.NP = this.navParams.data
    this.storage.get('liveSports').then((data) =>{
      this.liveSports = data
    } );


  }


}
