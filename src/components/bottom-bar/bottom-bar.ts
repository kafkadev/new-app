import { Component } from '@angular/core';
import { Events } from 'ionic-angular';
import { ModalController, NavParams } from 'ionic-angular';
import * as state from "../../interfaces/state-manage";
import * as _ from 'underscore';


@Component({
  selector: 'bottom-bar',
  templateUrl: 'bottom-bar.html'
})
export class BottomBarComponent {
  count: number = 0
  state: any = state
  constructor(
    public events: Events,
    public modalCtrl: ModalController,
  ) {
    this.events.subscribe('coupon:oddupdate', () => {
      this.count = Object.keys(state.CCT).length
    });
  }

  openModal() {
    const profileModal = this.modalCtrl.create("CouponsPage");
    profileModal.present();
  }


}
