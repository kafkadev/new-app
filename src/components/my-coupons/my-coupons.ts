import { Component, OnInit } from '@angular/core';
import { ServiceApiProvider } from '../../providers/service-api/service-api';

@Component({
  selector: 'my-coupons',
  templateUrl: './my-coupons.html'
})
export class MyCouponsComponent implements OnInit {
  myCoupons: any
  wallet: any = {}
  constructor(private serviceApi: ServiceApiProvider) {
    this.initPage()
  }
  ngOnInit() { }

  initPage() {
    /*
    let d = new Date()
    let today = d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate()
    let onemonth = d.getFullYear() + '-' + (d.getMonth() + 2) + '-' + d.getDate()
 */
    this.serviceApi.getApi(`my/coupons?duello=0&status=`)
      .subscribe((data) => {
        this.myCoupons = data.coupons
      })

    this.serviceApi.getApi('my/wallet')
      .subscribe((data) => {
        this.wallet = data
      })
  }

}
