import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from './../pipes/pipes.module';
import { IonicModule } from 'ionic-angular';
import { LiveComponent } from './live/live';
import { BottomBarComponent } from './bottom-bar/bottom-bar';
import { LiveListComponent } from './live-list/live-list';
import { PrematchListComponent } from './prematch-list/prematch-list';
import { MyCouponsComponent } from './my-coupons/my-coupons';
import { RootMenuComponent } from './root-menu/root-menu';
import { LiveInlistComponent } from './live-inlist/live-inlist';
import { PrematchTournamentsComponent } from './prematch-tournaments/prematch-tournaments';
import { SportsMenuComponent } from './sports-menu/sports-menu.component';
import { PrematchCategoriesComponent } from './prematch-categories/prematch-categories';
import { PrematchSportsComponent } from './prematch-sports/prematch-sports';
import { LiveDetailComponent } from './live-detail/live-detail';
import { PreDetailComponent } from './pre-detail/pre-detail';
import { TicketsComponent } from './tickets/tickets';
@NgModule({
	declarations: [
		LiveComponent,
		BottomBarComponent,
		LiveListComponent,
		PrematchListComponent,
		MyCouponsComponent,
		RootMenuComponent,
    LiveInlistComponent,
    PrematchTournamentsComponent,
    SportsMenuComponent,
    PrematchCategoriesComponent,
    PrematchSportsComponent,
    LiveDetailComponent,
    PreDetailComponent,
    TicketsComponent
],
	imports: [CommonModule, IonicModule, PipesModule],
	exports: [
		LiveComponent,
		BottomBarComponent,
		LiveListComponent,
		PrematchListComponent,
		MyCouponsComponent,
		RootMenuComponent,
    LiveInlistComponent,
    PrematchTournamentsComponent,
    SportsMenuComponent,
    PrematchCategoriesComponent,
    PrematchSportsComponent,
    LiveDetailComponent,
    PreDetailComponent,
    TicketsComponent]
})
export class ComponentsModule { }
