import { Component, Input, SimpleChanges, OnChanges } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavParams, Events } from 'ionic-angular';
import * as _ from 'underscore';
import * as LS from "../../interfaces/live-state";
import { GeneralProvider } from './../../providers/general/general';
import { ActionCoupons } from "../../providers/actions-coupons";
import * as state from "../../interfaces/state-manage";
@Component({
  selector: 'live-list',
  templateUrl: 'live-list.html',
})
export class LiveListComponent implements OnChanges {
  un = _
  limit: any = {}
  expanded: any = {};
  state: any = state
  @Input() headerColor: string = '#000';
  @Input() textColor: string = '#FFF';
  @Input() contentColor: string = '#fff';
  @Input() title: string = '';
  @Input() hasMargin: boolean = true;
  @Input() liveList: any;
  @Input() itemType: string;
  @Input() updatedLive: any;
  @Input() liveSports: any
  @Input() LiveCounts: any = {};


  constructor(
    public ACP: ActionCoupons,
    public events: Events,
    public GP: GeneralProvider,
    public storage: Storage
  ) {
    /*
    this.storage.get('liveStarted').then((value) => {
          this.liveList = _.groupBy(value, 'sport_id')
        });
    */
  }



  toggleAccordion(sport_id) {
    this.limit[sport_id] = 5
    this.expanded[sport_id] = !this.expanded[sport_id]
  }

  limitUp(sport_id) {
    let sportCount = LS.LiveCounts.sports[sport_id]
    if (sportCount > 5 && (sportCount - this.limit[sport_id])) {
      this.limit[sport_id] += 5
    }
  }

  getDbData() { }

  ngOnChanges(changes: SimpleChanges) {
    /* if (changes['liveData']) {
       this.liveList = changes['liveData']['currentValue'];
     }
 */
    for (let propName in changes) {
      if (propName === 'LiveCounts') {
        let sport_id = _.keys(changes[propName].currentValue.sports)[0]
        this.limit[sport_id] = 5
        this.expanded[sport_id] = !this.expanded[sport_id]
      }
    }



  }

}
