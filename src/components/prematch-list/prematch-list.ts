import { Component, Input, OnInit } from '@angular/core';
import { GeneralProvider } from './../../providers/general/general';
import { ActionCoupons } from "../../providers/actions-coupons";
import * as state from "../../interfaces/state-manage";
import * as _ from 'underscore';

@Component({
  selector: 'prematch-list',
  templateUrl: 'prematch-list.html'
})
export class PrematchListComponent {
  @Input() headerColor: string = '#000';
  @Input() textColor: string = '#FFF';
  @Input() contentColor: string = '#fff';
  @Input() title: string = 'demo accordion';
  @Input() hasMargin: boolean = true;
  @Input() items: any = {};
  @Input() itemType: string;
  expanded: any = {};
  state: any = state
  viewHeight: number = 100;
  un = _
  updated = {}

  constructor(
    public ACP : ActionCoupons,
    public GP: GeneralProvider) { }


  toggleAccordion(exp) {

    this.expanded[exp] = !this.expanded[exp]
  }



}
