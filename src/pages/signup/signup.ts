import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { NavController } from 'ionic-angular';

import { UserData } from '../../providers/user-data';

import { UserOptions } from '../../interfaces/user-options';


@Component({
  selector: 'page-user',
  templateUrl: 'signup.html'
})
export class SignupPage {
  signup: UserOptions
  submitted = false;

  constructor(public navCtrl: NavController, public userData: UserData) {
    localStorage.setItem('locale', 'tr-TR');
  }

  onSignup(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.userData.signup(this.signup.login);
      this.navCtrl.push('AccountPage');
    }
  }
}
