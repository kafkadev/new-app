import { Component } from '@angular/core';
import { NavParams, Events } from 'ionic-angular';
import * as state from "../../interfaces/state-manage";
import * as _ from 'underscore';

@Component({
  selector: 'page-sports',
  templateUrl: 'sports.html',
})
export class SportsPage {
  segment:any = 'live';
  state:any = state
  un:any = _
  constructor(
    public events: Events,
    public navParams: NavParams,
  ) {}

  updateSchedule(): void {
    if (this.segment === 'live') {
     // console.log(this.segment);
      this.events.publish('socket:open')
    } else {
     // console.log(this.segment);
      this.events.publish('socket:close')
    }
  }


}
