import { Component } from '@angular/core';
import { PopoverController, MenuController } from 'ionic-angular';
import { ActionCoupons } from "../../providers/actions-coupons";
import { PopoverPage } from '../about-popover/about-popover';
import * as langs from "./../../interfaces/langs";
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  conferenceDate = '2047-05-17';
  langs : any = langs
  constructor(
    public popoverCtrl: PopoverController,
    public actionCoupons : ActionCoupons,
    public menuCtrl: MenuController
  ) {
    console.log(this.actionCoupons.currentCoupons);

  }

  presentPopover(event: Event) {
    let popover = this.popoverCtrl.create(PopoverPage);
    popover.present({ ev: event });
  }
  acBeni = () => {
    this.menuCtrl.open();
  }
}
