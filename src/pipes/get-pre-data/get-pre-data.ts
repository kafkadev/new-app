import { Pipe, PipeTransform } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NP } from "../../interfaces/state-manage";
import * as _ from 'underscore';
@Pipe({
  name: 'getPreData',
})
export class GetPreDataPipe implements PipeTransform {
  constructor(
    public storage: Storage
  ) { }
  transform(value:string, anyid:number, limit:number = 10) {
    let resValue: any
    if (_.isString(value)) {
      switch (value) {
        case 'pre:sports':
        resValue = this.storage.get('preSports').then((preSports) => {
          return _.values(preSports)
        })
        break;
        case 'pre:sports:matches':
        resValue = this.storage.get('preMatches').then((preMatches) => {
          let getMatches = _.filter(preMatches, (match, i) => {
            return match.sport_id == anyid
          })
          return getMatches.slice(0, limit)
        })
        break;
        case 'preUpcoming':
          resValue = this.storage.get('preUpcoming').then((preUpcoming) => {
            return preUpcoming;
          })
          break;
        case 'pretour:keys':
        if (anyid) {
          resValue = this.storage.get('preTournaments').then((preTour) => {
              let getTours = _.filter(preTour, (tour) => {
                return tour.category == anyid
              })
              return getTours
          })
        } else {
          return 0
        }
          break;
        case 'precat':
        //console.log(_.has(NP.data, "sportid"));

        if (_.has(NP.data, "sportid")) {
          resValue = this.storage.get('preCategories').then((preCat) => {
              let getSports = _.groupBy(preCat, 'sport')[NP.data['sportid']]
              let sportKeys = _.sortBy(getSports, 'sort')
             // console.log(sportKeys);
              return sportKeys
          })
        } else {
          resValue = []
        }
          break;
        case 'pretour':
          resValue = this.storage.get('preMatches').then((preUpcoming) => {
            if (_.has(NP.data, "categoryid")) {
              let getTours = _.filter(preUpcoming, (match) => {
                return match.tournament_id == anyid
              })
              return getTours
            }
            else if (_.has(NP.data, "categoryid")) {
              let getSports = _.groupBy(preUpcoming, 'category_id')[46]
              return _.values(getSports)
            }
            else if (_.has(NP.data, "sportid")) {
              let getCategories = _.filter(preUpcoming, (match) => {
                return match.category_id == anyid
              })


             // let getCategories = _.groupBy(preUpcoming, 'category_id')[anyid]
              return getCategories.slice(0, 10)
            } else {
              return []
            }


          })
          break;
      }
    }
    return resValue;
  }
}
