import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from 'ionic-angular';
import { LangsFilterPipe } from './langs-filter/langs-filter';
import { GetDataPipe } from './get-data/get-data';
import { GetLiveDataPipe } from './get-live-data/get-live-data';
import { GetPreDataPipe } from './get-pre-data/get-pre-data';

@NgModule({
	declarations: [LangsFilterPipe,
		GetDataPipe,
    GetLiveDataPipe,
    GetPreDataPipe],
	imports: [CommonModule, IonicModule],
	exports: [LangsFilterPipe,
		GetDataPipe,
    GetLiveDataPipe,
    GetPreDataPipe]
})

export class PipesModule { }
