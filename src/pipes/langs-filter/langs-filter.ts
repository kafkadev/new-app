import { Pipe, PipeTransform } from '@angular/core';
import { LM, LO, LS, LC, LT } from './../../interfaces/langs';
import { LangGameStatuses } from './../../interfaces/lang-game-statuses';
import { LangSports } from './../../interfaces/lang-sports';
import { LangCategories } from './../../interfaces/lang-categories';
import { LangTournaments } from './../../interfaces/lang-tournaments';
import { LangMarkets } from './../../interfaces/lang-markets';
import { LangOdds } from './../../interfaces/lang-odds';



import * as _ from 'underscore';
@Pipe({
  name: 'langsFilter',
})
export class LangsFilterPipe implements PipeTransform {
  transform(value: any, type: any, page : any, subtype : any = 0) {
   // console.log(value);
//console.log(LangTournaments);

    let resp = value
    switch (type) {
      case 0:
      //SPORTS
        if (_.has(LangSports, value) && LangSports[value]['tr']) {
          resp = LangSports[value]['tr']
        }
        break;
      case 1:
      //CATEGORIES
        if (_.has(LangCategories, value) && LangCategories[value]['tr']) {
          resp = LangCategories[value]['tr']
        }
        break;
      case 2:
      //TOURNAMENTS
        if (_.has(LangTournaments, value) && LangTournaments[value]['tr']) {
          resp = LangTournaments[value]['tr']
        }
        break;
      case 3:
      //MARKETS
        if (_.has(LangMarkets, page) && _.has(LangMarkets[page], value) && LangMarkets[page][value]) {
          if (page == 'live') {
            resp = LangMarkets[page][value][subtype]
          } else {
            resp = LangMarkets[page][value]
          }
        }
        break;
      case 4:
      //ODDS
        if (_.has(LangOdds, page) && _.has(LangOdds[page], value) && LangOdds[page][value]) {
          resp = LangOdds[page][value]
        }
        break;
        case 5:
        if (_.has(value, 'tr')) {
          resp = value['tr']
        }
        break;
        //STATUSES
        case 6:
        //ODDS
          if (_.has(LangGameStatuses, value) && _.has(LangGameStatuses[value], 'tr')) {
            resp = LangGameStatuses[value]['tr']
          }
          break;
      default:
        return resp;
    }

    return resp;
  }
}
