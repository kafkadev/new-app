import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable'
import { Subject } from 'rxjs/Subject';
import { Events, App } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServiceApiProvider } from './../service-api/service-api';
import { UserData } from './../user-data';
import { ActionCoupons } from './../actions-coupons';
import { SocketService } from './../socket.service';
import { GeneralProvider } from './../general/general';
import * as state from "../../interfaces/state-manage";

@Injectable()
export class EventProvider {
  private subject = new Subject<any>();
  constructor(
    private serviceApi: ServiceApiProvider,
    private userData: UserData,
    private actionCoupons: ActionCoupons,
    private socketService: SocketService,
    private GP: GeneralProvider,
    public events: Events,
    public app: App,
    public storage: Storage) {
    this.listen()
    this.socketListen()
    this.userListen()
    this.navListen()
  }

  listen() {
/*
    this.listenMe().subscribe((data:any) => {
      Object.assign(state.NP, data.data.NP)
    })

    */
    this.events.subscribe('init:app', () => {
      this.GP.initAppComp();
      this.GP.initSportsComp();
      this.GP.initLiveComp();
    });
  }



  userListen() {
    this.events.subscribe('user:logout', () => {
      this.userData.logout();
    });

    this.events.subscribe('user:login', () => {
      // this.userData.logout();
    });
  }


  socketListen() {
    this.events.subscribe('socket:open', () => {
      this.socketService.close()
      this.socketService.sendMessage('betting')
    });

    this.events.subscribe('socket:close', () => {
      this.socketService.close()
    });

  }

  navListen() {
    this.events.subscribe('nav:update', (params) => {
      if (params['liveid']) {
        this.GP.getDetails(params.liveid, 'liveid')
      } else if (params['preid']) {
        this.GP.getDetails(params.preid, 'preid')
      }
    });
  }


  sendMessage(data: any = 0) {
    this.subject.next({data : state});
  }

  listenMe(): Observable<any> {
    /* return new Observable(observer => {
       setInterval(() => {
           observer.next(42);
       }, 1000);*/
    return this.subject.asObservable();
  }

  getData(): Promise<any> {
    return new Promise((resolve, reject) => {
      resolve(state)
    })
  }


}
