import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable'
import { LiveStarted, LiveUpdated, LiveSocket } from "../../interfaces/live-state";
import * as state from "../../interfaces/state-manage";
import { Subject } from 'rxjs/Subject';
import * as _ from 'underscore';
import * as Rx from 'rxjs/Rx';
declare var window: any;

@Injectable()
export class WebsocketService {
  private ws = new WebSocket(window.SOCKET_URL)
  private subject = new Subject<any>();
  constructor() {
setTimeout(() => {
  this.ws.onmessage = (evt: any) => {
    this.subject.next(JSON.parse(evt.data));
  };
}, 5000);

    /*
this.listenSubject().subscribe((data) => {
console.log(data);
})*/
  }

  sendMessage(data: any = 0) {
    this.subject.next({ data: data });
  }


  startSocket() {



    this.listenMe().subscribe((evt: any) => {
console.log('start socket listenMe');

      let SD = _.values(evt.data) //SOCKET DATA
      // console.log(JSON.parse(evt.data).data);

      for (let SI of SD) { // SI is Socket Item
        let ci = SI.details ? SI.details.matchid : false


        if (LiveStarted[ci]) {
          //  console.log(ci);
          //console.log("2");
          // console.log(SI.details);
          LiveSocket[ci] = SI.details

          Object.assign(LiveStarted[ci], SI.details);
          //Object.assign(LiveStarted[ci], SI.details);
          //kupon kontrolü yap
          if (SI.markets && LiveStarted[ci].markets && SI.betstatus !== 'stopped') {

            let MO = LiveStarted[ci].markets
            let MN = SI.markets

            for (var key in MO) {
              // this.liveStarted[ci].markets[key].active = MN[key].active

              if (MN[key] && MN[key].active === "0") {
                LiveStarted[ci].markets[key].active = "0"
              }
              else if (MO.hasOwnProperty(key) && MN[key]) {
                if (state.CCT && state.CCT[key]) {
                  console.log("Kupon İçinde Var - ", state.CCT[key]);

                }
                // console.log("Market Var 999999999");
                if (MO[key].odds && MN[key].odds && Array.isArray(MO[key].odds)) {
                  MO[key].odds.map((odd, i) => {
                    if (!_.isUndefined(MN[key].odds[odd.type])) {

                      let NODD = MN[key].odds[odd.type]
                      if (odd.value < NODD.value) {
                        MN[key].odds[odd.type]['action'] = 'oup';
                        LiveUpdated[key + NODD.typeid] = Date.now()
                      }
                      else if (odd.value > NODD.value) {
                        MN[key].odds[odd.type]['action'] = 'odown';
                        LiveUpdated[key + NODD.typeid] = Date.now()
                      }
                    }

                  })
                  MN[key].odds = _.values(MN[key].odds)
                  Object.assign(LiveStarted[ci].markets[key], MN[key])
                  // console.log(this.liveStarted[ci].markets[key]);
                }
              }
            }
          }
        }
      }
      // console.log(LiveSocket);

      setInterval(() => {
        if (_.size(LiveUpdated)) {
          for (var index in LiveUpdated) {
            if (LiveUpdated[index] < Date.now() - 3000) {
              delete LiveUpdated[index]
            }
          }
        }
      }, 3000)


    })
  }


  listenMe(): Observable<any> {
    /* return new Observable(observer => {
       let num = 1
       if (ws.readyState === WebSocket.OPEN) {

         ws.onmessage = (evt: any) => {
           this.subject.next(JSON.parse(evt.data));
           observer.next(JSON.parse(evt.data));
         };
         console.log("ready state");

       }


     })*/
     console.log('ready socket')


    return this.subject.asObservable();
  }

  getData(daa): Promise<any> {
    return new Promise((resolve, reject) => {
      resolve(daa)
    })
  }

  listenSubject(): Observable<any> {
    return this.subject.asObservable();
  }

}
