import { Injectable, Inject } from '@angular/core';
import { App, NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import * as LD from './../../interfaces/live-state';
import { PreSports, PreCurrent, PreTournaments, PreCounts, PreCategories, PreUpcoming } from './../../interfaces/pre-state';
import { ServiceApiProvider } from './../service-api/service-api';
import { LangSports } from './../../interfaces/lang-sports';
import { LangMarkets } from './../../interfaces/lang-markets';
import { LangOdds } from './../../interfaces/lang-odds';
import * as state from "./../../interfaces/state-manage";
import * as langs from "./../../interfaces/langs";
import * as _ from 'underscore';

@Injectable()
export class GeneralProvider {
  constructor(
    private app: App,
    private serviceApi: ServiceApiProvider,
    public storage: Storage
  ) { }



  initAppComp(): any {
    //Odds
    langs.LO['live'] = LangOdds.live
    langs.LO['prematch'] = LangOdds.prematch
    //Markets
    langs.LM['live'] = LangMarkets.live
    langs.LM['prematch'] = LangMarkets.prematch
    //Other
    langs.LS['prematch'] = LangSports
  }

  initSportsComp(): any {
    this.serviceApi
      .getLogin('sportsbook')
      .subscribe((data) => {
        const allMatches = data.pre_matches
        const counts = {
          sports: _.countBy(allMatches, 'sport_id'),
          categories: _.countBy(allMatches, 'category_id'),
          tournaments: _.countBy(allMatches, 'tournament_id')
        }
        this.storage.set('preCounts', counts);
        Object.assign(PreCounts, counts)

        this.storage.set('preMatches', allMatches);
        this.storage.set('preSports', data.pre_sports);
        this.storage.set('preCategories', data.pre_categories);
        this.storage.set('preTournaments', data.pre_tournaments);
        this.storage.set('preUpcoming', _.values(allMatches).slice(0, 150));


      })
  }

  initLiveComp(): any {
    this.serviceApi
      .getApi('live')
      .subscribe(res => {
        const { liveData, sports, categories, tournaments } = res
        Object.assign(LD.LiveStarted, liveData)
        Object.assign(LD.LiveSports, sports)
        Object.assign(LD.LiveCategories, categories)
        Object.assign(LD.LiveTournaments, tournaments)
        //---
        this.storage.set('liveStarted', liveData);
        this.storage.set('liveSports', sports);
        this.storage.set('liveCategories', categories);
        this.storage.set('liveTournaments', tournaments);
        //---
        const countSport = _.countBy(liveData, 'sport_id')
        Object.assign(LD.LiveSportsCount, countSport)
        this.storage.set('liveSportCount', countSport);

        const counts = {
          sports: _.countBy(liveData, 'sport_id'),
          categories: _.countBy(liveData, 'category_id'),
          tournaments: _.countBy(liveData, 'tournament_id')
        }
        Object.assign(LD.LiveCounts, counts)
        this.storage.set('LiveCounts', counts);
      })
  }


  getDetails(anyid: number, type: any) {
    switch (type) {
      case 'liveid':
        this.serviceApi
          .getApi('live/detail/' + anyid)
          .subscribe((data) => {
            if (_.has(LD, 'LiveStarted') && _.has(LD.LiveStarted, anyid)) {
              Object.assign(LD.LiveStarted[anyid], data.data[anyid])
            }
          })
        break;
      case 'preid':
        this.serviceApi
          .getApi('sportsbook/detail/' + anyid)
          .subscribe((data) => {
            console.log(data);
            if (data.data == 0) {
              this.storage.get('preMatches').then((resDb) => {
                PreCurrent[anyid] = resDb[anyid]
              });
            }
            else if (data.data !== 0 && data.status === 'success') {
              PreCurrent[anyid] = data.data
            }
          })
        break;
      default:
        break;
    }
  }


  navPush = (page: any, id: number) => {
    switch (page) {
      case 'liveid':
        this.navCtrl.setRoot('LiveDetail', { liveid: id, type: page });
        break;
      case 'preid':
        this.navCtrl.setRoot('PreDetail', { preid: id, type: page });
        break;
      case 'categoryid':
        this.navCtrl.setRoot('CategoryDetail', { categoryid: id, type: page });
        break;
      case 'sportid':
        this.navCtrl.setRoot('SportDetail', { sportid: id, type: page });
        break;
      default:
        break;
    }
  }


  get navCtrl(): any {
    return this.app.getRootNavs()[0];
  }
}
