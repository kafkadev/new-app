export const LangGameStatuses : any = {
    "not_started": {
      "tr": "Başlamadı",
      "en": "Not started",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "1map": {
      "tr": "1. Harita",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "2map": {
      "tr": "2. Harita",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "1q": {
      "tr": "1. Çeyrek",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "2q": {
      "tr": "2. Çeyrek",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "3q": {
      "tr": "3. Çeyrek",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "4q": {
      "tr": "4. Çeyrek",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "5q": {
      "tr": "5. Çeyrek",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "1p": {
      "tr": "1. Yarı",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "2p": {
      "tr": "2. Yarı",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "1set": {
      "tr": "1. Set",
      "en": "1st set",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "2set": {
      "tr": "2. Set",
      "en": "2nd set",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "3set": {
      "tr": "3. Set",
      "en": "3rd set",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "4set": {
      "tr": "4. Set",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "5set": {
      "tr": "5. Set",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "golden_set": {
      "tr": "Altın Set",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "in_progress": {
      "tr": "Başlıyor",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "paused": {
      "tr": "Ara",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "halftime": {
      "tr": "İlk Yarı",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "overtime": {
      "tr": "Uzatmalar",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "first_half_ot": {
      "tr": "Uz. 1. Yarı ",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "second_half_ot": {
      "tr": "Uz. 2. Yarı ",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "pen": {
      "tr": "Penaltılar",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "postponed": {
      "tr": "Ertelendi",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "delayed": {
      "tr": "Gecikti",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "cancelled": {
      "tr": "İptal",
      "en": "Cancelled",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "interrupted": {
      "tr": "Yarıda Kaldı",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "stopped": {
      "tr": "Durdu",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "abandoned": {
      "tr": "Terkedildi",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "walkover": {
      "tr": "Galibiyet",
      "en": "Walkover",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "retired": {
      "tr": "Bitti",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "walkover1": {
      "tr": "Galibiyet",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "walkover2": {
      "tr": "Galibiyet",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "retired1": {
      "tr": "Bitti",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "retired2": {
      "tr": "Bitti",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "defaulted1": {
      "tr": "Bitti 1",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "defaulted2": {
      "tr": "Bitti 2",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "ended": {
      "tr": "Maç Sonucu",
      "en": "Ended",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "awaiting_ot": {
      "tr": "Uz. Bekleniyor",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "ot_ht": {
      "tr": "Uz. Ara",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "awaiting_pen": {
      "tr": "Pen. Bekleniyor",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "after_ot": {
      "tr": "Uz. Sonucu",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "after_penalties": {
      "tr": "Pen Sonucu",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "first_pause": {
      "tr": "1. Duraklama",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "second_pause": {
      "tr": "2. Duraklama",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "third_pause": {
      "tr": "3. Duraklama",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "fourth_pause": {
      "tr": "4. Duraklama",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "1p_ot": {
      "tr": "1. Yarı Uztm.",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "2p_ot": {
      "tr": "2. Yarı Uztm.",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "ass": {
      "tr": "A.s.s",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    },
    "removed": {
      "tr": "Kaldırıldı",
      "en": "ee",
      "de": "",
      "ru": "",
      "aa": ""
    }
  }
  