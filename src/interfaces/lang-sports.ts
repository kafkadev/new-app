export const LangSports : any = {
  "1": {
      "id": "1",
      "total": "880",
      "tr": "Futbol",
      "en": "Soccer",
      "de": "Fußball",
      "ru": "Футбол",
      "aa": "كرة القدم",
      "sort": "1"
  },
  "2": {
      "id": "2",
      "total": "201",
      "tr": "Basketbol",
      "en": "Basketball",
      "de": "Basketball",
      "ru": "Баскетбол",
      "aa": "كرة السلة",
      "sort": "2"
  },
  "4": {
      "id": "4",
      "total": "130",
      "tr": "Buz Hokeyi",
      "en": "Ice Hockey",
      "de": "Eishockey",
      "ru": "Хоккей",
      "aa": "هوكي الجليد",
      "sort": "14"
  },
  "5": {
      "id": "5",
      "total": "95",
      "tr": "Tenis",
      "en": "Tennis",
      "de": "Tennis",
      "ru": "Теннис",
      "aa": "كرة المضرب",
      "sort": "3"
  },
  "6": {
      "id": "6",
      "total": "70",
      "tr": "Hentbol",
      "en": "Handball",
      "de": "Handball",
      "ru": "Гандбол",
      "aa": "كرة اليد",
      "sort": "5"
  },
  "10": {
      "id": "10",
      "total": "9",
      "tr": "Boks",
      "en": "Boxing",
      "de": "Boxen",
      "ru": "Бокс",
      "aa": "الملاكمة",
      "sort": "12"
  },
  "11": {
      "id": "11",
      "total": "30",
      "tr": "Motor Sporları",
      "en": "Motorsport",
      "de": "Motorsport",
      "ru": "Автоспорт",
      "aa": "رياضة المحركات",
      "sort": "13"
  },
  "12": {
      "id": "12",
      "total": "23",
      "tr": "Ragbi",
      "en": "Rugby",
      "de": "Rugby",
      "ru": "Регби",
      "aa": "رجبي",
      "sort": "9"
  },
  "16": {
      "id": "16",
      "total": "15",
      "tr": "Amerikan Futbolu",
      "en": "American Football",
      "de": "American Football",
      "ru": "Американский футбол",
      "aa": "كرة القدم",
      "sort": "7"
  },
  "19": {
      "id": "19",
      "total": "1",
      "tr": "Snooker",
      "en": "Snooker",
      "de": "Snooker",
      "ru": "Снукер",
      "aa": "سنوكر",
      "sort": "15"
  },
  "20": {
      "id": "20",
      "total": "8",
      "tr": "Masa Tenisi",
      "en": "Table Tennis",
      "de": "Tischtennis",
      "ru": "Настольный теннис",
      "aa": "كرة الطاولة",
      "sort": "16"
  },
  "21": {
      "id": "21",
      "total": "8",
      "tr": "Kriket",
      "en": "Cricket",
      "de": "Cricket",
      "ru": "Крикет",
      "aa": "كريكيت",
      "sort": "17"
  },
  "22": {
      "id": "22",
      "total": "48",
      "tr": "Dart",
      "en": "Darts",
      "de": "Darts",
      "ru": "Дартс",
      "aa": "رمي السهام",
      "sort": "11"
  },
  "23": {
      "id": "23",
      "total": "120",
      "tr": "Voleybol",
      "en": "Volleyball",
      "de": "Volleyball",
      "ru": "Волейбол",
      "aa": "كرة الطائرة",
      "sort": "4"
  },
  "26": {
      "id": "26",
      "total": "2",
      "tr": "Sutopu",
      "en": "Waterpolo",
      "de": "Wasserball",
      "ru": "Водное поло",
      "aa": "البولو المائي",
      "sort": "19"
  },
  "29": {
      "id": "29",
      "total": "21",
      "tr": "Salon Futbolu",
      "en": "Futsal",
      "de": "Futsal",
      "ru": "Футзал",
      "aa": "كرة قدم الصالات",
      "sort": "20"
  },
  "31": {
      "id": "31",
      "total": "10",
      "tr": "Badminton",
      "en": "Badminton",
      "de": "Badminton",
      "ru": "Бадминтон",
      "aa": "البادمنتون",
      "sort": "21"
  },
  "60": {
      "id": "60",
      "total": "0",
      "tr": "Plaj Futbolu",
      "en": "Beach Soccer",
      "de": "Beachsoccer",
      "ru": "Пляжный футбол",
      "aa": "كرة قدم الشاطئية",
      "sort": "24"
  },
  "7": {
      "id": "7",
      "total": "27",
      "tr": "Floorball",
      "en": "Floorball",
      "de": "Unihockey",
      "ru": "Флорбол",
      "aa": "لعبة كرة الارض",
      "sort": "28"
  },
  "15": {
      "id": "15",
      "total": "14",
      "tr": "Bandy",
      "en": "Bandy",
      "de": "Bandy",
      "ru": "Хоккей с мячом",
      "aa": "هوكي الجليد",
      "sort": "999"
  },
  "109": {
      "id": "109",
      "tr": "Counter-Strike",
      "en": "Counter-Strike",
      "de": "Counter-Strike",
      "ru": "Контр-Страйк",
      "aa": "Counter-Strike",
      "sort": "5"
  },
  "110": {
      "id": "110",
      "tr": "League of Legends",
      "en": "League of Legends",
      "de": "League of Legends",
      "ru": "Лига Легенд",
      "aa": "League of Legends",
      "sort": "6"
  },
  "111": {
      "id": "111",
      "tr": "Dota",
      "en": "Dota 2",
      "de": "Dota",
      "ru": "Дота",
      "aa": "Dota 2",
      "sort": "6"
  },
  "37": {
      "id": "37",
      "total": "0",
      "tr": "Squash",
      "en": "Squash",
      "de": "Squash",
      "ru": "Сквош",
      "aa": "الاسكواش",
      "sort": "27"
  },
  "43": {
      "id": "43",
      "total": "64",
      "tr": "Alp Disiplini",
      "en": "Alpine Skiing",
      "de": "Ski Alpin",
      "ru": "Горные лыжи",
      "aa": "لتزلج على المنحدرات الجليدية",
      "sort": null
  }
}
